import { NgModule } from '@angular/core';
import { CdkTableModule } from "@angular/cdk/table";
import { MatToolbarModule } from "@angular/material/toolbar";
import { CdkTreeModule } from "@angular/cdk/tree";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatIconModule } from "@angular/material/icon";
import { ObserversModule } from "@angular/cdk/observers";
import { MatInputModule } from "@angular/material/input";
import { PlatformModule } from "@angular/cdk/platform";
import { BidiModule } from "@angular/cdk/bidi";
import { OverlayModule } from "@angular/cdk/overlay";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatMenuModule } from "@angular/material/menu";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { MatBottomSheetModule } from "@angular/material/bottom-sheet";
import { CdkStepperModule } from "@angular/cdk/stepper";
import { MatCardModule } from "@angular/material/card";
import { MatTabsModule } from "@angular/material/tabs";
import { A11yModule } from "@angular/cdk/a11y";
import { PortalModule } from "@angular/cdk/portal";


@NgModule({
  exports: [
    // CDK
    A11yModule,
    BidiModule,
    ObserversModule,
    OverlayModule,
    PlatformModule,
    PortalModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    // Material
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatBottomSheetModule,
    MatTabsModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatIconModule,
    MatMenuModule
  ]
})
export class MaterialModule { }
