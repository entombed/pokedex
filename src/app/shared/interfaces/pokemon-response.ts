export interface PokemonResponse {
  count: number,
  next: string,
  previous: null,
  results: PokemonList[]
}

export interface PokemonList {
  name: string,
  url: string,
  sprites: any,
  types: any
}
