import { Inject, Injectable, InjectionToken } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PokemonDetail } from '../shared/interfaces/pokemon-detail';
import { PokemonList, PokemonResponse } from '../shared/interfaces/pokemon-response';

export const BASE_URL = new InjectionToken<string>('BASE_URL');

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  protected baseUrl!: string;

  constructor(protected httpClient: HttpClient, @Inject(BASE_URL) url: string) {
    this.baseUrl = url;
  }

  public getPokemonList(offset: number, limit: number = 20): Observable<PokemonList[]> {
    return this.httpClient
      .get<PokemonResponse>(
        `${this.baseUrl}/pokemon?offset=${offset}&limit=${limit}`
      )
      .pipe(
        map((data: PokemonResponse) => data.results)
      );
  }

  public getPokemonDetail(pokemon: number | string): Observable<PokemonDetail> {
    return this.httpClient.get<PokemonDetail>(`${this.baseUrl}/pokemon/${pokemon}`);
  }
}
