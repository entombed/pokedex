import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../services/pokemon.service';
import { FormControl } from '@angular/forms';
import { PokemonList } from '../../shared/interfaces/pokemon-response';
import { PokemonDetail, Type } from '../../shared/interfaces/pokemon-detail';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { PokemonDetailComponent } from '../pokemon-detail/pokemon-detail.component';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.sass'],
})
export class PokemonListComponent implements OnInit {
  public search: FormControl = new FormControl();
  public pokemons: PokemonList[] = [];
  public searchPokemon: PokemonDetail = {} as PokemonDetail;
  public classicMode = true;
  public isSearching!: boolean;
  public isLoading!: boolean;
  public isLastPage!: boolean;
  private offset = 0;

  constructor(
    private pokemonService: PokemonService,
    private bottomSheet: MatBottomSheet,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getPage(this.offset);
  }

  getPage(offset: number) {
    if (!this.isLoading && !this.isLastPage) {
      this.isLoading = true;
      this.pokemonService
        .getPokemonList(offset)
        .subscribe((list: PokemonList[]) => {
          if (list.length === 0) {
            this.isLastPage = true;
          }

          if (!this.isLastPage) {
            this.getPokemon(list);
          }
        });
    }
  }

  onSearchPokemon(): void {
    const value = this.search.value;
    if (value === '') {
      this.isSearching = false;
    } else {
      this.isSearching = true;
      this.isLoading = true;
      this.pokemonService.getPokemonDetail(value).subscribe(
        (pokemon: PokemonDetail) => {
          this.searchPokemon = pokemon;
          this.isLoading = false;
        },
        (error: any) => {
          this.isLoading = false;
          if (error.status === 404) {
            this.snackBar.open('Sorry, Pokemon not found', 'Ok', {
              duration: 5000,
            });
          }
        }
      );
    }
  }

  onScroll(event: Event): void {
    const element: HTMLDivElement = event.target as HTMLDivElement;
    if (element.scrollHeight - element.scrollTop < 1000) {
      this.getPage(this.offset);
    }
  }

  private getPokemon(list: PokemonList[]) {
    const arr: Observable<PokemonDetail>[] = [];
    list.map((value: PokemonList) => {
      arr.push(this.pokemonService.getPokemonDetail(value.name));
    });

    forkJoin([...arr]).subscribe((pokemons: any[]) => {
      this.pokemons.push(...pokemons);
      this.offset += 20;
      this.isLoading = false;
    });
  }

  getPrincipalType(list: Type[] | undefined) {
    return (list as Type[]).filter((date: Type) => date.slot === 1)[0]?.type.name;
  }

  onDetail(pokemon: any): void {
    this.bottomSheet.open(PokemonDetailComponent, {
      data: { pokemon, classicMode: this.classicMode },
    });
  }
}
