import { Component, Inject } from '@angular/core';
import { PokemonDetail } from '../../shared/interfaces/pokemon-detail';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.sass'],
})
export class PokemonDetailComponent {
  pokemon: PokemonDetail = {} as PokemonDetail;
  classicMode!: boolean;

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any
  ) {
    this.pokemon = data.pokemon;
    this.classicMode = data.classicMode;
  }


  getAbilities(): string {
    return this.pokemon.abilities.map(x => x.ability.name).join(', ');
  }

  getPrincipalType(list: any[]) {
    return list.filter(x => x.slot === 1)[0]?.type.name;
  }
}
